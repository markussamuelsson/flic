#import <UIKit/UIKit.h>
#import "FlicHandler.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) FlicHandler *flicHandler;

@end

