//
//  FlicHandler.m
//  FlicGrabberExample
//
//  Created by Englund, Markus (94773) on 2/8/16.
//  Copyright © 2016 My company. All rights reserved.
//

#import "FlicHandler.h"

#define SCL_APP_ID @"cfe4b5bb-ba39-4e51-808a-f9dcaad86341"
#define SCL_APP_SECRET @"bf6b88da-3c46-46be-bfdb-b3135819183a"

static NSString *key = @"Counter";

@interface FlicHandler () <SCLFlicManagerDelegate, SCLFlicButtonDelegate>

@property (nonatomic, strong) NSMutableArray<SCLFlicButton *> *flicButtons;

@end

@implementation FlicHandler

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.flicButtons = [[NSMutableArray<SCLFlicButton *> alloc] init];
        self.flicManager = [[SCLFlicManager alloc] initWithDelegate:self appID:SCL_APP_ID appSecret:SCL_APP_SECRET backgroundExecution:YES andRestoreState:YES];
    }
    return self;
}



// This button setup is called each time a button is selected from the Flic app AND when
// after restoring a button
-(void)setupFlicButton:(SCLFlicButton *)flicButton
{
    NSLog(@"%s", __FUNCTION__);
    flicButton.delegate = self;
    flicButton.triggerBehavior = SCLFlicButtonTriggerBehaviorClickAndDoubleClickAndHold;
    flicButton.mode = SCLFlicButtonModeBackground;
    [flicButton connect];
}

-(void)refreshPendingConnections
{
    [self.flicManager refreshPendingConnections];
    [self scheduleNotificationWithTitle:@"refreshPendingConnections" button:nil];
}

-(void)forgetAllButtons
{
    [self.flicButtons enumerateObjectsUsingBlock:^(SCLFlicButton * _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.flicManager forgetButton:button];
    }];
    [self.flicButtons removeAllObjects];
}

#pragma mark - Setup Flic button via Flic App

-(void)requestButtonFromFlicApp
{
    [self.flicManager requestButtonFromFlicAppWithCallback:@"externalflictest://button"];
}

- (BOOL)handleOpenURL:(NSURL *)url;
{
    NSError *error = nil;
    SCLFlicButton *flicButton = [self.flicManager generateButtonFromURL:url error:&error];
    //If an existing button is selected, do we need to call [flicManager forgetButton]? If YES, we have a chicken and egg problem
    //since we only know which button to forget after calling generateButtonFromURL.
    //[self.flicManager forgetButton:flicButton];

    if(flicButton)
    {
        [self setupFlicButton:flicButton];
        [self.flicButtons addObject:flicButton];
        return YES;
    }
    else
    {
        NSString *title = [NSString stringWithFormat:@"Failed to generate button: %@", [self stringFromError:error]];
        NSLog(@"%@", title);
        [self scheduleNotificationWithTitle:title button:nil];
        return NO;
    }
}

#pragma mark - SCLFlicManagerDelegate methods

- (void)flicManagerDidRestoreState:(SCLFlicManager *)manager;
{
    NSLog(@"%s", __FUNCTION__);
    self.flicButtons = [manager.knownButtons.allValues mutableCopy];
    [self.flicButtons enumerateObjectsUsingBlock:^(SCLFlicButton * _Nonnull flicButton, NSUInteger idx, BOOL * _Nonnull stop) {
        //The only thing we have to do here is to set the delegate /Flic
        flicButton.delegate = self;
    }];
}

-(void)flicManager:(SCLFlicManager *)manager didChangeBluetoothState:(SCLFlicManagerBluetoothState)state
{
    NSLog(@"%s - %@", __FUNCTION__, [self bluetoothStateString:state]);
    NSString *title = [NSString stringWithFormat:@"didChangeBluetoothState - %@", [self bluetoothStateString:state]];
    [self scheduleNotificationWithTitle:title button:nil];
}

-(void)flicManager:(SCLFlicManager *)manager didForgetButton:(NSUUID *)buttonIdentifier error:(NSError *)error
{
    NSLog(@"%s", __FUNCTION__);
    NSString *title = [NSString stringWithFormat:@"didForgetButton - %@", [self stringFromError:error]];
    [self scheduleNotificationWithTitle:title button:nil];
    
}

#pragma mark - SCLFlicButtonDelegate methods

- (void)flicButton:(SCLFlicButton *)button didReceiveButtonDown:(BOOL) queued age:(NSInteger) age;
{
    NSInteger counter = [[NSUserDefaults standardUserDefaults] integerForKey:key];
    counter++;
    [[NSUserDefaults standardUserDefaults] setInteger:counter forKey:key];
    
    if (self.buttonDown)
    {
        self.buttonDown();
    }
}

-(void)flicButton:(SCLFlicButton *)button didReceiveButtonClick:(BOOL)queued age:(NSInteger)age
{
    NSLog(@"%s", __FUNCTION__);
    [self scheduleNotificationWithTitle:@"Click" button:button];
}

-(void)flicButton:(SCLFlicButton *)button didReceiveButtonDoubleClick:(BOOL)queued age:(NSInteger)age
{
    NSLog(@"%s", __FUNCTION__);
    [self scheduleNotificationWithTitle:@"Double Click" button:button];
}

-(void)flicButton:(SCLFlicButton *)button didReceiveButtonHold:(BOOL)queued age:(NSInteger)age
{
    NSLog(@"%s", __FUNCTION__);
    [self scheduleNotificationWithTitle:@"Hold" button:button];
}

-(void)flicButton:(SCLFlicButton *)button didDisconnectWithError:(NSError *)error
{
    NSLog(@"%s, %@", __FUNCTION__, [self stringFromError:error]);
    NSString *title = [NSString stringWithFormat:@"didDisconnectWithError: %@", [self stringFromError:error]];
    [self scheduleNotificationWithTitle:title button:button];
}

-(void)flicButton:(SCLFlicButton *)button didFailToConnectWithError:(NSError *)error
{
    NSLog(@"%s, %@", __FUNCTION__, [self stringFromError:error]);
    NSString *title = [NSString stringWithFormat:@"didFailToConnectWithError: %@", [self stringFromError:error]];
    [self scheduleNotificationWithTitle:title button:button];
    // TODO: Anton: Only in this case will the flicManager not reconnect automatically..!
    
}

-(void)flicButtonDidConnect:(SCLFlicButton *)button
{
    NSLog(@"%s", __FUNCTION__);
    [self scheduleNotificationWithTitle:@"flicButtonDidConnect" button:button];
}

-(void)flicButtonIsReady:(SCLFlicButton *)button
{
    NSLog(@"%s", __FUNCTION__);
    [self scheduleNotificationWithTitle:@"flicButtonIsReady" button:button];
}

#pragma mark - 

//Log with Local Notifications so we know what's going on...
-(void)scheduleNotificationWithTitle:(NSString *)title button:(SCLFlicButton *)button
{
    NSString *buttonIdentifier = button.name ?: @"";
    
    NSInteger counter = [[NSUserDefaults standardUserDefaults] integerForKey:key];
    
    UILocalNotification *not = [[UILocalNotification alloc] init];
    not.alertTitle = [NSString stringWithFormat:@"Flic: %@ (%@ %@)", title, buttonIdentifier, @(counter)];
    
    NSString *date = [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterLongStyle];
    not.alertBody = [NSString stringWithFormat:@"%@ (%@)\n%@ %@", title, @(counter), buttonIdentifier, date];
    not.fireDate = [NSDate date];
    
    [[UIApplication sharedApplication] scheduleLocalNotification:not];
}

#pragma mark - Strings

-(NSString *)connectionStateString:(SCLFlicButtonConnectionState)state
{
    switch (state) {
        case SCLFlicButtonConnectionStateConnected:
            return @"Connected";
            break;
        case SCLFlicButtonConnectionStateConnecting:
            return @"Connecting";
            break;
        case SCLFlicButtonConnectionStateDisconnected:
            return @"Disconnected";
            break;
        case SCLFlicButtonConnectionStateDisconnecting:
            return @"Disconnecting";
            break;
    }
}

-(NSString *)modeString:(SCLFlicButtonMode)mode
{
    switch (mode) {
        case SCLFlicButtonModeForeground:
            return @"Foreground";
            break;
        case SCLFlicButtonModeBackground:
            return @"Background";
            break;
    }
}

-(NSString *)buttonsInfo
{
    if (self.flicButtons.count == 0)
    {
        return @"No buttons found";
    }
    
    NSMutableString *info = [NSMutableString string];
    
    [info appendFormat:@"Enabled: %@\n", @(_flicManager.enabled)];
    [info appendString:@"===========\n"];
    
    [self.flicButtons enumerateObjectsUsingBlock:^(SCLFlicButton * _Nonnull flicButton, NSUInteger idx, BOOL * _Nonnull stop) {
        [info appendFormat:@"%@", [self buttonInfo:flicButton]];
        [info appendString:@"===========\n"];
    }];
    return info;
}

-(NSString *)buttonInfo:(SCLFlicButton *)button
{
    NSMutableString *info = [NSMutableString string];
    //    [info appendFormat:@"buttonIdentifier: %@\n", button.buttonIdentifier];
    //    [info appendFormat:@"buttonPublicKey: %@\n", button.buttonPublicKey];
    [info appendFormat:@"Name: %@\n", button.name];
    [info appendFormat:@"UserAssignedName: %@\n", button.userAssignedName];
    [info appendFormat:@"connectionState: %@\n", [self connectionStateString:button.connectionState]];
    [info appendFormat:@"mode: %@\n", [self modeString:button.mode]];
    [info appendFormat:@"isReady: %@\n", @(button.isReady)];
    //    [info appendFormat:@"triggerBehavior: %@\n", @(button.triggerBehavior)];
    //    [info appendFormat:@"pressCount: %@\n",  @(button.pressCount)];
    return info;
}

-(NSString *)bluetoothStateString:(SCLFlicManagerBluetoothState)state
{
    switch (state) {
        case SCLFlicManagerBluetoothStatePoweredOn:
            return @"PoweredOn";
            break;
        case SCLFlicManagerBluetoothStatePoweredOff:
            return @"PoweredOff";
            break;
        case SCLFlicManagerBluetoothStateResetting:
            return @"Resetting";
            break;
        case SCLFlicManagerBluetoothStateUnsupported:
            return @"Unsupported";
            break;
        case SCLFlicManagerBluetoothStateUnauthorized:
            return @"Unauthorized";
            break;
        case SCLFlicManagerBluetoothStateUnknown:
            return @"Unknown";
            break;
    }
}

-(NSString *)stringFromError:(NSError *)error
{
    if (error == nil)
    {
        return @"OK";
    }
    
    SCLFlicError code = error.code;
    
    switch (code)
    {
        case SCLFlicErrorUnknown:
            return @"Unknown";
            break;
        case SCLFlicErrorCouldNotCompleteTask:
            return @"CouldNotCompleteTask";
            break;
        case SCLFlicErrorConnectionFailed:
            return @"ConnectionFailed";
            break;
        case SCLFlicErrorCouldNotUpdateRSSI:
            return @"CouldNotUpdateRSSI";
            break;
        case SCLFlicErrorDatabaseError:
            return @"DatabaseError";
            break;
        case SCLFlicErrorUnknownDataReceived:
            return @"UnknownDataReceived";
            break;
        case SCLFlicErrorVerificationTimeOut:
            return @"VerificationTimeOut";
            break;
        case SCLFlicErrorBackendUnreachable:
            return @"BackendUnreachable";
            break;
        case SCLFlicErrorNoInternetConnection:
            return @"NoInternetConnection";
            break;
        case SCLFlicErrorCredentialsNotMatching:
            return @"CredentialsNotMatching";
            break;
        case SCLFlicErrorButtonIsPrivate:
            return @"ButtonIsPrivate";
            break;
        case SCLFlicErrorCryptographicFailure:
            return @"CryptographicFailure";
            break;
        case SCLFlicErrorButtonDisconnectedDuringVerification:
            return @"ButtonDisconnectedDuringVerification";
            break;
        case SCLFlicErrorMissingData:
            return @"MissingData";
            break;
        case SCLFlicErrorInvalidSignature:
            return @"InvalidSignature";
            break;
        case SCLFlicErrorButtonAlreadyGrabbed:
            return @"ButtonAlreadyGrabbed";
            break;
        case SCLFlicErrorBluetoothErrorUnknown:
            return @"BluetoothErrorUnknown";
            break;
        case SCLFlicErrorBluetoothErrorInvalidParameters:
            return @"BluetoothErrorInvalidParameters";
            break;
        case SCLFlicErrorBluetoothErrorInvalidHandle:
            return @"BluetoothErrorInvalidHandle";
            break;
        case SCLFlicErrorBluetoothErrorNotConnected:
            return @"BluetoothErrorNotConnected";
            break;
        case SCLFlicErrorBluetoothErrorOutOfSpace:
            return @"BluetoothErrorOutOfSpace";
            break;
        case SCLFlicErrorBluetoothErrorOperationCancelled:
            return @"BluetoothErrorOperationCancelled";
            break;
        case SCLFlicErrorBluetoothErrorConnectionLost:
            return @"BluetoothErrorConnectionLost";
            break;
        case SCLFlicErrorBluetoothErrorPeripheralDisconnected:
            return @"BluetoothErrorPeripheralDisconnected";
            break;
        case SCLFlicErrorBluetoothErrorUUIDNotAllowed:
            return @"BluetoothErrorUUIDNotAllowed";
            break;
        case SCLFlicErrorBluetoothErrorAlreadyAdvertising:
            return @"BluetoothErrorAlreadyAdvertising";
            break;
        case SCLFlicErrorBluetoothErrorConnectionFailed:
            return @"BluetoothErrorConnectionFailed";
            break;
        case SCLFlicErrorBluetoothErrorConnectionLimitReached:
            return @"BluetoothErrorConnectionLimitReached";
            break;
    }
    
    return [NSString stringWithFormat:@"Uknown error for code: %@", @(code)];
}

#pragma mark - 

-(void)enable
{
    [_flicManager enable];
}

-(void)disable
{
    [_flicManager disable];
}

-(void)connectAllButtons
{
    [self.flicButtons enumerateObjectsUsingBlock:^(SCLFlicButton * _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
        [button connect];
    }];
}

-(void)disconnectAllButtons
{
    [self.flicButtons enumerateObjectsUsingBlock:^(SCLFlicButton * _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
        [button disconnect];
    }];
}

@end
