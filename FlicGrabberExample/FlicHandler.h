//
//  FlicHandler.h
//  FlicGrabberExample
//
//  Created by Englund, Markus (94773) on 2/8/16.
//  Copyright © 2016 My company. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <fliclib/fliclib.h>
#import "ViewController.h"

/**
 The FlicHandler is responsible for connecting multiple Flic Buttons.
 It handles single, double and hold clicks, and displays local notifications for these actions.
 It supports background mode.
 */
@interface FlicHandler : NSObject

@property (nonatomic, strong) SCLFlicManager *flicManager;

-(BOOL)handleOpenURL:(NSURL *)url;
-(void)requestButtonFromFlicApp;
-(void)refreshPendingConnections;

/*
 A callback that will be called at ButtonDown.
 */
@property (nonatomic, copy) void (^buttonDown)();

/**
 Returns some debug info about connected buttons
 */
-(NSString *)buttonsInfo;

-(void)forgetAllButtons;

-(void)enable;
-(void)disable;
-(void)connectAllButtons;
-(void)disconnectAllButtons;

@end
