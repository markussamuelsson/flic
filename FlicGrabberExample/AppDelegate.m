#import "AppDelegate.h"
#import "ViewController.h"
@import CoreLocation;

@interface AppDelegate ()<CLLocationManagerDelegate>
@property (nonatomic, strong) CLLocationManager *locationManager;
@end

@implementation AppDelegate
{
    FlicHandler *_flicHandler;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //Location manager
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
    {
        [self.locationManager startMonitoringSignificantLocationChanges];
    }
    else
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.locationManager requestAlwaysAuthorization];
        });
    }

    //Flic hanlder instance that will live as long as the appliation is alive in memory
    _flicHandler = [[FlicHandler alloc] init];
    
    
    //Notification setup
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert categories:nil];
    [application registerUserNotificationSettings:settings];

    NSLog(@"launchOptions: %@", launchOptions);
    
    UILocalNotification *not = [[UILocalNotification alloc] init];
    not.alertTitle = [NSString stringWithFormat:@"launchOptions: %@", launchOptions];
    not.alertBody = [NSString stringWithFormat:@"launchOptions: %@", launchOptions];
    not.fireDate = [NSDate date];
    [[UIApplication sharedApplication] scheduleLocalNotification:not];
    
	return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(nonnull NSURL *)url sourceApplication:(nullable NSString *)sourceApplication annotation:(nonnull id)annotation;
{
    return [_flicHandler handleOpenURL:url];
}

-(void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"%s", __FUNCTION__);
}

#pragma mark - CLLocationManagerDelegate methods

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status;
{
    if(status == kCLAuthorizationStatusAuthorizedAlways)
    {
        [self.locationManager startMonitoringSignificantLocationChanges];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations;
{
    [_flicHandler refreshPendingConnections];
}



@end