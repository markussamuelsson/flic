#import "ViewController.h"
#import "FlicHandler.h"
#import "AppDelegate.h"


@interface ViewController ()

@property (nonatomic, strong) IBOutlet UIImageView *flappyFlic;
@property (nonatomic, strong) IBOutlet UIButton *grabFlic;
@property (nonatomic, strong) IBOutlet UILabel *flicName;
- (IBAction)forgetAllPressed:(id)sender;

@property (nonatomic) CGFloat offset;
@property (nonatomic) CGFloat velocity;
@property (nonatomic) CGFloat roof;

@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (nonatomic, strong) FlicHandler *flicHandler;
- (IBAction)pressedEnable:(id)sender;
- (IBAction)pressedDisable:(id)sender;
- (IBAction)pressedConnect:(id)sender;
- (IBAction)pressedDisconnect:(id)sender;

@end

@implementation ViewController
{
    NSTimer *_refreshTimer;
}

#pragma mark - Life Cycle

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    AppDelegate *appDel = (id)[UIApplication sharedApplication].delegate;
    self.flicHandler =  [appDel flicHandler];
    __weak typeof(self) weakSelf = self;
    self.flicHandler.buttonDown = ^(){
        [weakSelf jump];
    };
	self.flappyFlic.layer.magnificationFilter = kCAFilterNearest;
	[NSTimer scheduledTimerWithTimeInterval:1.0/60.0 target:self selector:@selector(step) userInfo:nil repeats:YES];
    self.textView.text = @"Press refresh for button info";
    self.textView.scrollEnabled = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _refreshTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateDebugInfo) userInfo:nil repeats:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [_refreshTimer invalidate];
    [super viewWillDisappear:animated];
}

- (void)viewDidLayoutSubviews;
{
	self.roof = self.flappyFlic.frame.origin.y - 100;
}

//#pragma mark - IBActions
- (IBAction)grab:(id)sender;
{
    [self.flicHandler requestButtonFromFlicApp];
}

#pragma mark - Private methods

- (void)jump;
{
	self.velocity = 20.0;
}

- (void)step;
{
	self.offset += self.velocity;
	self.velocity -= 1.5;
	
	if(self.offset < 0.0)
	{
		self.offset = 0.0;
	}
	else if(self.offset > self.roof)
	{
		self.offset = self.roof;
		self.velocity = 0.0;
	}
	
	self.flappyFlic.layer.transform = CATransform3DMakeTranslation(0.0, -self.offset, 0.0);
}

-(void)updateDebugInfo
{
    self.textView.text = [self.flicHandler buttonsInfo];
}

- (IBAction)forgetAllPressed:(id)sender
{
    [_flicHandler forgetAllButtons];
}

- (IBAction)pressedEnable:(id)sender
{
    [_flicHandler enable];
}

- (IBAction)pressedDisable:(id)sender
{
    [_flicHandler disable];
}

- (IBAction)pressedConnect:(id)sender
{
    [_flicHandler connectAllButtons];
}

- (IBAction)pressedDisconnect:(id)sender
{
    [_flicHandler disconnectAllButtons];
}

@end
